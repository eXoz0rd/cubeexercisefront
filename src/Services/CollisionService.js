import { AbstractService } from "@/Abstract/AbstractService";

export class CollisionService extends AbstractService {
  constructor() {
    super();
    this.controllerUrl = "Collision";
  }

  async getAllCurrentCollisions() {
    const url = `${this.controllerUrl}/CalculateCollisions`;
    return await this.get(url);
  }
}
