import { AbstractService } from "@/Abstract/AbstractService";

export class ShapesService extends AbstractService {
  constructor() {
    super();
    this.controllerUrl = "Shape";
  }

  async getAllCurrentShapes() {
    const url = `${this.controllerUrl}/ReadAll`;
    return await this.get(url);
  }

  async addNewShape(data) {
    const url = `${this.controllerUrl}/Create`;
    return await this.post(url, data);
  }
}
