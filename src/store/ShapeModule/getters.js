const getters = {
  getAllCurrentShapes: state => {
    return state.currentShapes;
  }
};

export default getters;
