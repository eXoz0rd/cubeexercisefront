import { ShapeModel } from "@/Models/ShapeModel";

const mutations = {
  addShape(state, payload) {
    if (
      typeof payload === "undefined" ||
      payload === null ||
      payload.length < 1
    )
      return;
    const model = new ShapeModel(
      payload.id,
      payload.position,
      payload.dimensions,
      payload.shapeType
    );
    if (
      !state.currentShapes.some(shape => {
        return shape.id === model.id;
      })
    ) {
      state.currentShapes.push(model);
    } else {
      const shape = state.currentShapes.find(shape => {
        return shape.id === model.id;
      });
      const index = state.currentShapes.indexOf(shape);
      ``;
      state.currentShapes[index] = model;
    }
  }
};

export default mutations;
