import { ShapesService } from "@/Services/ShapesService";

const actions = {
  async loadAllCurrentShapes({ commit }) {
    try {
      const service = new ShapesService();
      const response = await service.getAllCurrentShapes();
      if (
        typeof response === "undefined" ||
        response === null ||
        response.length
      )
        return;
      for (const shape of response.data) {
        commit("addShape", shape);
      }
    } catch (e) {
      throw new Error(`Could not load shapes. ${e}`);
    }
  },

  // eslint-disable-next-line no-unused-vars
  async addNewShape({ commit, dispatch }, payload) {
    //TODO prepare data
    try {
      const service = new ShapesService();
      await service.addNewShape(payload);
      dispatch("loadAllCurrentShapes");
      await dispatch("loadAllCurrentCollisions");
    } catch (e) {
      throw new Error(`Could not add new shape. ${e}`);
    }
  }
};
export default actions;
