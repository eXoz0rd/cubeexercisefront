import { CollisionTableModel } from "@/Models/TableModels/CollisionTableModel";

const mutations = {
  addCollision(state, payload) {
    if (
      typeof payload === "undefined" ||
      payload === null ||
      payload.length < 1
    )
      return;

    state.currentCollisions = [];
    for (const collision of payload) {
      const model = new CollisionTableModel(
        collision.id,
        collision.intersectionArea,
        collision.firstShape,
        collision.secondShape
      );
      state.currentCollisions.push(model);
    }
  }
};

export default mutations;
