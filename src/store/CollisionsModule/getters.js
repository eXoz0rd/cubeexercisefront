const getters = {
  getCurrentCollisions: state => state.currentCollisions || []
};
export default getters;
