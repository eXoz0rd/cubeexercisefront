import { CollisionService } from "@/Services/CollisionService";

const actions = {
  async loadAllCurrentCollisions({ commit }) {
    const service = new CollisionService();
    try {
      const response = await service.getAllCurrentCollisions();
      commit("addCollision", response.data);
    } catch (e) {
      throw new Error(`Could not load collisions: ${e}`);
    }
  }
};

export default actions;
