export class ShapeModel {
  constructor(id, position, dimensions, shapeType) {
    this.id = id;
    this.position = position;
    this.dimensions = dimensions;
    this.shapeType = shapeType;
  }
}
