export class Dimensions {
  constructor(length, height, width) {
    this.length = length;
    this.height = height;
    this.width = width;
  }
}
