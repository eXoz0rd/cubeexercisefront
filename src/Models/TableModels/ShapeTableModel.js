export class ShapeTableModel {
  constructor(id, dimensions, position, shapeType) {
    this.Id = id;
    this.PositionX = position.x;
    this.PositionY = position.y;
    this.PositionZ = position.z;
    this.Length = dimensions.length;
    this.Width = dimensions.width;
    this.Height = dimensions.height;
    this.ShapeType = shapeType;
  }
}
