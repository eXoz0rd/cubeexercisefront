import { ShapeTableModel } from "@/Models/TableModels/ShapeTableModel";

export class CollisionTableModel {
  constructor(id, intersectionArea, firstShape, secondShape) {
    this.intersectionArea = intersectionArea;
    this.id = id;
    this.shapes = [];
    this.shapes.push(
      new ShapeTableModel(
        firstShape.id,
        firstShape.dimensions,
        firstShape.position,
        firstShape.shapeType
      ),
      new ShapeTableModel(
        secondShape.id,
        secondShape.dimensions,
        secondShape.position,
        secondShape.shapeType
      )
    );
  }
}
