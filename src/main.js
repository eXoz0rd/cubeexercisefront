import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import "./quasar";
import ShapeModule from "@/store/ShapeModule/ShapeModule";
import CollisionsModule from "@/store/CollisionsModule/CollisionsModule";

Vue.config.productionTip = false;
Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    shapeModule: ShapeModule,
    collisionsModule: CollisionsModule
  }
});

new Vue({
  render: h => h(App),
  store: store
}).$mount("#app");
