import Vue from "vue";

import "./styles/quasar.sass";
import Notify from "quasar/src/plugins/Notify";
import iconSet from "quasar/icon-set/mdi-v4";
import "@quasar/extras/material-icons-outlined/material-icons-outlined.css";
import "@quasar/extras/material-icons-round/material-icons-round.css";
import "@quasar/extras/material-icons-sharp/material-icons-sharp.css";
import "@quasar/extras/material-icons/material-icons.css";
import "@quasar/extras/mdi-v4/mdi-v4.css";
import {
  Quasar,
  QLayout,
  QHeader,
  QDrawer,
  QPageContainer,
  QPage,
  QToolbar,
  QToolbarTitle,
  QBtn,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel,
  QSelect,
  QCheckbox,
  QForm,
  QInput,
  QTable,
  QSpace,
  QTr,
  QTd,
  QTooltip
} from "quasar";

Vue.use(Quasar, {
  config: {},
  components: {
    QLayout,
    QHeader,
    QDrawer,
    QPageContainer,
    QPage,
    QToolbar,
    QToolbarTitle,
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel,
    QSelect,
    QCheckbox,
    QForm,
    QInput,
    QTable,
    QSpace,
    QTr,
    QTd,
    QTooltip
  },
  directives: {},
  extras: [
    "material-icons",
    "material-icons-outlined",
    "material-icons-round",
    "material-icons-sharp",
    "mdi-v3",
    "ionicons-v4",
    "eva-icons",
    "fontawesome-v5",
    "themify",
    "line-awesome"
  ],
  plugins: {
    Notify
  },
  framework: {
    iconSet: "mdi-v4"
  },
  iconSet: iconSet
});
