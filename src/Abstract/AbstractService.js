import axios from "axios";

export class AbstractService {
  constructor(host = "https://localhost:44340") {
    this.host = host;
  }

  async post(url, data, config = null) {
    return await axios.post(`${this.host}/${url}`, data, config);
  }

  async get(url, config = null) {
    return await axios.get(`${this.host}/${url}`, config);
  }
}
