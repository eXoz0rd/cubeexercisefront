<template>
  <q-table
    title="Shapes"
    :data="this.shapesData"
    :columns="columns"
    row-key="id"
    :hide-bottom="header"
    :loading="loading"
  >
    <template v-slot:top-right>
      <div v-if="!header" class="q-gutter-sm">
        <q-btn
          @click="expandMenu"
          round=""
          class="bg-transparent text-positive"
          icon="mdi-plus"
        >
          <q-tooltip content-class="bg-positive">
            Add new shape
          </q-tooltip>
        </q-btn>
        <q-btn
          @click="refreshTable"
          round
          class="bg-transparent text-positive"
          icon="mdi-refresh"
        >
          <q-tooltip content-class="bg-positive">
            Refresh table
          </q-tooltip>
        </q-btn>
      </div>
    </template>
  </q-table>
</template>

<script>
import { ShapeTypes } from "@/Enums/ShapeTypes";

export default {
  name: "AddedShapesTable",
  loading: false,
  props: ["hideHeader", "shapesData"],
  data() {
    return {
      header: false,
      loading: false,
      columns: [
        {
          name: "Id",
          label: "Id",
          align: "left",
          field: "Id"
        },
        {
          name: "PositionX",
          align: "center",
          label: "Position (X)",
          field: "PositionX",
          sortable: true
        },
        {
          name: "PositionY",
          label: "Position (Y)",
          field: "PositionY",
          sortable: true
        },
        {
          name: "PositionZ",
          label: "Position (Z)",
          field: "PositionZ",
          sortable: true
        },
        { name: "Length", label: "Length", field: "Length", sortable: true },
        { name: "Width", label: "Width", field: "Width", sortable: true },
        {
          name: "Height",
          label: "Height",
          field: "Height",
          sortable: true
        },
        {
          name: "ShapeType",
          label: "Shape Type",
          field: "ShapeType",
          format: val =>
            `${Object.keys(ShapeTypes).find(key => {
              return ShapeTypes[key] === val;
            })}`,
          sortable: true
        }
      ]
    };
  },
  methods: {
    expandMenu() {
      this.$root.$emit("expandMenu");
    },
    async refreshTable() {
      const self = this;
      this.loading = true;
      try {
        await this.$store.dispatch("loadAllCurrentShapes");
      } catch (e) {
        self.$q.notify({
          type: "negative",
          message: `${e}`
        });
      } finally {
        this.loading = false;
      }
    }
  },
  created() {
    if (typeof this.hideHeader === "undefined" || this.hideHeader === null)
      return;
    this.header = this.hideHeader;
  }
};
</script>

<style scoped></style>
